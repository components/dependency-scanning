# SBOM generator used for License and Dependency Scanning

These components are used to generate a CycloneDX Software Bill of Materials which is then used by GitLab to identify a project's licenses and vulnerable components. The SBOM generated complies with the [GitLab taxonomy](https://docs.gitlab.com/ee/development/sec/cyclonedx_property_taxonomy.html).

The epic https://gitlab.com/groups/gitlab-org/-/epics/7288 will increase language coverage by introducing new Components. While it's in progress, some Components in this repository may also generate a [GitLab security report](https://docs.gitlab.com/ee/development/integrations/secure.html#report) instead of/addition to the CycloneDX SBOM.

The CI/CD Components on this repository require
[Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/), a
GitLab [Ultimate](https://about.gitlab.com/pricing/feature-comparison/) feature.

## Components

| Target projects | Output | Maturity |
| ------ | ------ | ------ |
| [All](templates/main) | SBOM | [Beta](https://docs.gitlab.com/ee/policy/development_stages_support.html#beta) |
| [DEPRECATED] [Android](templates/android)             | SBOM and Security report | [Experiment](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment) |
| [DEPRECATED] [Rust (`Cargo.lock`)](templates/cargo)   | SBOM | [Beta](https://docs.gitlab.com/ee/policy/development_stages_support.html#beta) |
| [DEPRECATED] [Swift](templates/swift)             | SBOM and Security report | [Experiment](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment) |
| [DEPRECATED] [Cocoapods](templates/cocoapods)             | SBOM | [Experiment](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment) |

### Notes on maturity level

As part of https://gitlab.com/groups/gitlab-org/-/epics/14146, Components that currently generate a [security report](https://docs.gitlab.com/ee/development/integrations/secure.html#report) will be deprecated and replaced with SBOM integration only. Languages/package managers that do not have lock file support will be replaced with documentation on how to generate SBOMs using the language/package manager native tooling. The maturity level of these Components will remain at the `Experiment` level until then.

While we accept bug reports for `Experiment` Components, we are unlikely to prioritize work that involves build support for the target project because any issues in this area will be out-of-scope for the GitLab analyzers once https://gitlab.com/groups/gitlab-org/-/epics/14146 is delivered.

Components in `Beta` are largely stable and safe to use in production, however we may make breaking changes outside of major GitLab versions until they're promoted to [Generally Available](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#generally-available-ga). Given the support for [semantic version ranges](https://docs.gitlab.com/ee/ci/components/#semantic-version-ranges) in Components, this wouldn't affect users who pin to a major version (do not specify `~latest` or `main` as a Component version if you want to avoid breaking changes).

Components using [`gemnasium`](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#analyzers) analyzers will be updated to use the [software composition analyzer for lock files](https://gitlab.com/groups/gitlab-org/-/epics/14484), which is being developed as part of https://gitlab.com/groups/gitlab-org/-/epics/7288.

Only Components using the analyzer for lock files will be promoted to [Generally Available](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#generally-available-ga) maturity level.

## Contribute

1. Read how to [contribute to GitLab development](https://docs.gitlab.com/ee/development/contributing/) and the [Development guide for GitLab official CI/CD components](https://docs.gitlab.com/ee/development/cicd/components.html).
2. Submit a merge request, and follow the bot instructions.
