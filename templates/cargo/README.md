# Generate cargo SBOMs (DEPRECATED)

:warning: This CI/CD component is deprecated and replaced with the [main Dependency Scanning CI/CD component](../main/README.md).

## Example usage

```yaml
include:
  - component: gitlab.com/components/dependency-scanning/cargo@main
```
