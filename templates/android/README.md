# Android dependency scanning CI/CD component (DEPRECATED)

:warning: This CI/CD component is deprecated and replaced with the [main Dependency Scanning CI/CD component](../main/README.md).

## Summary

This component may be used for dependency scanning of Android projects. When included in a `.gitlab-ci.yml` file, the component adds a job titled `android-dependency-scanning` to the pipeline. The job generates a [Dependency Scanning report](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdependency_scanning) and a [CycloneDX SBOM report](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscyclonedx).

The component uses [GitLab dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) capabilities. All information documented regarding dependency scanning applies to the
Android dependency scanning component.

### Interaction with dependency scanning template

When using the [CI/CD template to run dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#enabling-the-analyzer-by-using-the-cicd-template), avoid using the Android dependency scanning component. The template supports Android scanning. Using both at the same time in the same stage may lead to the different tools overriding each other's results.

## Usage example

Add the following snippet to your `.gitlab-ci.yml` to run the job `android-dependency-scanning` in the `test` pipeline stage.

```yaml
include:
  - component: gitlab.com/components/android-dependency-scanning/component@<VERSION>
    inputs:
      stage: test
```

Make sure to set the component's version. Released versions may be found in the [tags section](https://gitlab.com/components/android-dependency-scanning/-/tags) of the project. More information on component versioning and available options may be found in [component versions documentation](https://docs.gitlab.com/ee/ci/components/#component-versions).
