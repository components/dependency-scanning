# Dependency scanning CI/CD component

## Summary

This component may be used for dependency scanning of projects.  When included
in a `.gitlab-ci.yml` file, the component adds a job titled
`dependency-scanning` to the pipeline.  The job generates a
[CycloneDX SBOM report](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscyclonedx).

The component uses [GitLab dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
capabilities.

### Interaction with dependency scanning template

Avoid using the [CI/CD template to run dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#edit-the-gitlab-ciyml-file-manually).
Using both is **not** supported, and may lead to unexpected and undesired results.

## Usage example

Add the following snippet to your `.gitlab-ci.yml` to run the `dependency-scanning` job in the `test` pipeline stage.

```yaml
include:
  - component: $CI_SERVER_FQDN/components/dependency-scanning/main@<VERSION>
    inputs:
      stage: post-build
```

Make sure to set the component's version. Released versions may be found in the
[tags section](https://gitlab.com/components/dependency-scanning/-/tags) of the
project. More information on component versioning and available options may be
found in [component versions documentation](https://docs.gitlab.com/ee/ci/components/#component-versions).
