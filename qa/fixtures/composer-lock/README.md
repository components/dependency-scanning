# composer-lock generation

You can generate the `composer.lock` by running `composer install`.
The `composer.json` contains the direct dependencies of the project.
