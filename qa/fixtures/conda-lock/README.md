# conda-lock

This directory contains a `conda-lock.yml` file generated from
`environment.yml` using

```shell
conda-lock lock --file environment.yml --platform linux-64
```
