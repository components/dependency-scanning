# go-graph

This project contains a `go.graph` file that can be regenerated from
`go.mod` by issuing `go mod graph > go.graph`.
