# pnpm lockfile generation

This project contains a `pnpm-lock.yaml` file which was generated with the following commands:

1. Create base project with `pnpm init`
1. Add normal dependency `pnpm add next@14.2.13`
1. Add dev dependency `pnpm add -D jest@29.7.0`
1. Check pnpm dependency tree via `pnpm list --depth 10`
