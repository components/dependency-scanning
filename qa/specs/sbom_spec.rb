require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

PROJECT_CONFIGS = {
  android: {
    project: "linkhub",
    target_dir: "qa/fixtures/linkhub",
    relative_sbom_paths: ["gl-sbom-maven-gradle.cdx.json", "app/gl-sbom-maven-gradle.cdx.json"]
  },
  swift: {
    project: "swift",
    target_dir: "qa/fixtures/swift",
    relative_sbom_paths: ["gl-sbom-swift-swift.cdx.json"]
  },
  cocoapods: {
    project: "cocoapods",
    target_dir: "qa/fixtures/cocoapods",
    relative_sbom_paths: ["gl-sbom-cocoapods-cocoapods.cdx.json"]
  },
  cargo:{
    project: "cargo",
    target_dir: "qa/fixtures/cargo",
    relative_sbom_paths: ["gl-sbom-cargo-cargo.cdx.json"]
  },
  "composer-lock": {
    project: "composer-lock",
    target_dir: "qa/fixtures/composer-lock",
    relative_sbom_paths: ["gl-sbom-packagist-composer.cdx.json"]
  },
  "go-mod": {
    project: "go-mod",
    target_dir: "qa/fixtures/go-mod",
    relative_sbom_paths: ["gl-sbom-go-go.cdx.json"]
  },
  "gradle-dependency-lock-plugin": {
    project: "gradle-dependency-lock-plugin",
    target_dir: "qa/fixtures/gradle-dependency-lock-plugin",
    relative_sbom_paths: ["gl-sbom-maven-gradle.cdx.json"]
  },
  "maven-dependency-tree": {
    project: "maven-dependency-tree",
    target_dir: "qa/fixtures/maven-dependency-tree",
    relative_sbom_paths: ["gl-sbom-maven-maven.cdx.json"]
  },
  "npm-lock": {
    project: "npm-lock",
    target_dir: "qa/fixtures/npm-lock",
    relative_sbom_paths: ["gl-sbom-npm-npm.cdx.json"]
  },
  "pnpm-lock": {
    project: "pnpm-lock",
    target_dir: "qa/fixtures/pnpm-lock",
    relative_sbom_paths: ["gl-sbom-npm-pnpm.cdx.json"]
  },
  "pip-compile": {
    project: "pip-compile",
    target_dir: "qa/fixtures/pip-compile",
    relative_sbom_paths: ["gl-sbom-pypi-pip.cdx.json"]
  },
  "poetry-lock": {
    project: "poetry-lock",
    target_dir: "qa/fixtures/poetry-lock",
    relative_sbom_paths: ["gl-sbom-pypi-poetry.cdx.json"]
  },
  "conda-lock": {
    project: "conda-lock",
    target_dir: "qa/fixtures/conda-lock",
    relative_sbom_paths: ["gl-sbom-conda-condalock.cdx.json"]
  },
  "go-graph": {
    project: "go-graph",
    target_dir: "qa/fixtures/go-graph",
    relative_sbom_paths: ["gl-sbom-go-go.cdx.json"]
  }
}.freeze

def run_sbom_tests(project_type)
  config = PROJECT_CONFIGS.fetch(project_type)

  describe "when scanning #{config[:project]}" do
    let(:project) { config[:project] }
    let(:expectations_dir) { "qa/expect" }

    scan = OpenStruct.new
    scan.target_dir = config[:target_dir]
    let(:scan) { scan }

    describe "CycloneDX SBOMs" do
      let(:relative_expectation_dir) { project }
      let(:relative_sbom_paths) { config[:relative_sbom_paths] }

      it_behaves_like "non-empty CycloneDX files"
      it_behaves_like "recorded CycloneDX files"
      it_behaves_like "valid CycloneDX files"
    end
  end
end

begin
  project_type = ENV.fetch('PROJECT_TYPE').to_sym
  run_sbom_tests(project_type)
rescue KeyError
  abort "Error: PROJECT_TYPE environment variable is not set or invalid. Valid types are: #{PROJECT_CONFIGS.keys}"
end
