require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe "when scanning maven-gradle-modules" do
  let(:project) { "linkhub" }
  let(:expectations_dir) { "qa/expect" }

  # Workaround cyclonedx_shared_examples expecting scan object
  scan = OpenStruct.new
  scan.target_dir = "qa/linkhub"
  let(:scan) { scan }

  describe "CycloneDX SBOMs" do
    let(:relative_expectation_dir) { project }
    let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json", "app/gl-sbom-maven-gradle.cdx.json"] }

    it_behaves_like "non-empty CycloneDX files"
    it_behaves_like "recorded CycloneDX files"
    it_behaves_like "valid CycloneDX files"
  end
end
